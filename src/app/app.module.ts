import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeHttpModule, NativeHttpBackend, NativeHttpFallback } from 'ionic-native-http-connection-backend';
import { Platform } from 'ionic-angular';
import { HttpBackend, HttpXhrBackend } from '@angular/common/http';
import { NativeHttpFallbackD, NativeHttpModuleD } from 'ionic-native-http-connection-backend';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/home/home';
import { CertificatePage } from '../pages/certificate/certificate';
import { CertificateViewPage } from '../pages/certificate-view/certificate-view';
import { ProfilePage } from '../pages/profile/profile';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { TabPage } from '../pages/tab/tab';
import { SettingPage } from '../pages/setting/setting';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AuthProvider } from '../providers/auth/auth';
import { RemoteServiceProvider } from '../providers/remote-service/remote-service';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    CertificatePage,
    CertificateViewPage,
    ProfilePage,
    DashboardPage,
    TabPage,
    SettingPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule,
    NativeHttpModule,
    NativeHttpModuleD
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    CertificatePage,
    CertificateViewPage,
    ProfilePage,
    DashboardPage,
    TabPage,
    SettingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}, 
    {provide: HttpBackend, useClass: NativeHttpFallback, deps: [Platform, NativeHttpBackend, HttpXhrBackend]},
    InAppBrowser,
    File,
    DocumentViewer,
    FileTransfer,
    SocialSharing,
    AuthProvider,
    HttpClientModule,
    HttpModule,
    RemoteServiceProvider,
  ]
})
export class AppModule {}
