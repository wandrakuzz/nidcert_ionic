import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CertificateViewPage } from '../certificate-view/certificate-view';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
import 'rxjs/add/operator/map';




/**
 * Generated class for the CertificatePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-certificate',
  templateUrl: 'certificate.html',
})

export class CertificatePage {
  profile: string = "information";
  
  profiles:any;
  url : string = "";

  viewCertificate = CertificateViewPage;
 
  constructor(public navCtrl: NavController, public navParams: NavParams,private remoteservice : RemoteServiceProvider) {
    this.getProfile();
  }
  getProfile(){
    this.remoteservice.getProfile().subscribe((data) => {
    
      this.profiles = data;
      this.url = "https://app2.nidcert.com/"+data.avatar_location;
      
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CertificatePage');
  }
 
}


