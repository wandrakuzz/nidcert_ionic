import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { TabPage } from '../tab/tab';
import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class LoginPage {

  tabPage = TabPage;

  // @ViewChild('username') username;
  // @ViewChild('password') password;

  constructor(
    public navCtrl: NavController, 
    public alertCtrl: AlertController,
    public auth : AuthProvider
  ) {
  }

  login(FormLogin){
    console.log(FormLogin.value);
    
    this.auth.login(FormLogin.value).subscribe(data => {
      
      if(data.success === true){
        this.navCtrl.setRoot(TabPage);
        let alert = this.alertCtrl.create({
          title : 'Login Successfully!.',
          subTitle: data.message,
          buttons : ['OK']
        });
        alert.present();
      }else 
        FormLogin.password = '';
        let alert = this.alertCtrl.create({
          title : 'Login Failed',
          subTitle: data.message,
          buttons : ['OK']
        });
        alert.present();
      
    })
  }

}
