import { Component } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { CertificatePage } from '../certificate/certificate';
import { ProfilePage } from '../profile/profile';
import { CertificateViewPage } from '../certificate-view/certificate-view';
import { File } from '@ionic-native/file';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FileTransfer } from '@ionic-native/file-transfer';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Headers, Http, Response } from '@angular/http';
import { RemoteServiceProvider } from '../../providers/remote-service/remote-service';
import 'rxjs/add/operator/map';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  certificatePage = CertificatePage;
  profilePage = ProfilePage;
  viewCertPage = CertificateViewPage;

  certificates: any;
  url: string = 'https://app2.nidcert.com/api/certificate/';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private document: DocumentViewer, 
    private file: File, 
    private transfer: FileTransfer, 
    public plt: Platform,
    private socialSharing: SocialSharing,
    public http: Http,
    private remoteservice : RemoteServiceProvider
  ) {
    this.getCertificates();
      
  }

  getCertificates(){
    this.remoteservice.getCertificates().subscribe((data) => {
      this.certificates = data;
    });

  }
  
  downloadAndOpenPdf() {
    let path = null;
    let id = window.localStorage.getItem('user_id');
    
    if (this.plt.is('ios')) {
      path = this.file.documentsDirectory;
    } else if (this.plt.is('android')) {
      path = this.file.dataDirectory;
    }
 
    const transfer = this.transfer.create();
    transfer.download('https://app2.nidcert.com/certificate/'+id, path + 'myfile.pdf').then(entry => {
      let url = entry.toURL();
      this.document.viewDocument(url, 'application/pdf', {});
    });
  }

    message:string = null;
    image:string = null;

    shareLink(){

        this.socialSharing.share(this.message,null, this.image, 'https://app2.nidcert.com/certificate/open_pdf/1').then(() => {
          // Success!
        }).catch(() => {
          // Error!
        });
        
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashboardPage');
  }

  private extreactData(res : Response){
    return res.json();
  }


}
