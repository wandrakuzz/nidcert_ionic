import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { CertificatePage } from '../certificate/certificate';
import { AuthProvider } from '../../providers/auth/auth';
import { LoginPage } from '../home/home';


/**
 * Generated class for the TabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {

  tab1Root = DashboardPage;
  tab2Root = CertificatePage;
  tab3Root = TabPage;

  constructor(public navCtrl: NavController, public navParams: NavParams,public auth : AuthProvider, private alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabPage');
  }

  logout(){
    let alert = this.alertCtrl.create({
      title: 'Log out confirmation',
      message: 'Do you want to logout?',
      buttons: [
        {
          text: 'Back',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.auth.logout();
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
    
  }

}
