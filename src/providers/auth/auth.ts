import { Http, Response, Headers  } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  baseUrl:string = "https://app2.nidcert.com/api/"
  
  constructor(public http: Http) {
    
  }

  createAuthorizationHeader(headers: Headers){
    headers.set('Authorization',window.localStorage.getItem('token')); 
    headers.set('Content-Type',window.localStorage.getItem('application/json')); 
    headers.set('X-Requested-With',window.localStorage.getItem('XMLHttpRequest')); 
    headers.set('user_id',window.localStorage.getItem('user_id'))
  }

  private(){
    let headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(this.baseUrl+'login',{
      headers : headers
    }).map(res => res.json());
  }

  login(data){

    return this.http.post(this.baseUrl+"login",data)
    .map(this.extracData);

    
  }

  isLogged(){
    if(window.localStorage.getItem('token')){
      return true
    } else{
      return false;
    }
  }

  logout(){
    window.localStorage.removeItem('token');
    return true;
  }

  private extracData(res : Response){
    let body = res.json();
  
    if(body.success === true){
      window.localStorage.setItem('token',body.data.token);
      window.localStorage.setItem('user_id',body.data.user_id[0]);
    };

    return body || {};
  }
}