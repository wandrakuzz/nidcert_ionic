import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import {Http ,Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the RemoteServiceProvider provider.
 
  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RemoteServiceProvider {

  private url: string = "https://app2.nidcert.com/api/certificate/";
  private urlProfile: string = "https://app2.nidcert.com/api/profile/";

  constructor(private http: Http) {
    console.log('Hello RemoteServiceProvider Provider');
  }

  getCertificates(){
    let id = window.localStorage.getItem('user_id');
    let token = window.localStorage.getItem('token');

    return this.http.get(this.url+id+"?token="+token)
  .do((res : Response) => res.json)
  .map((res : Response) => res.json().data);
  }

  getProfile(){

    let id = window.localStorage.getItem('user_id');
    let token = window.localStorage.getItem('token');

    return this.http.get(this.urlProfile+id+"?token="+token)
    .do((res : Response) => res.json)
    .map((res : Response) => res.json().data);
  }
  // getCertificates(){
  //   this.http.get(this.url)
  //   .do((res:Response) => console.log(res))
  //   .map(this.extreactData)
  //   .catch(this.catchError)
  // }

  // private catchError(error : Response | any){
  //   console.log(error);
  //   return Observable.throw(error.json().error || "Server Error")
  // }

  // private logResponse(res : Response){
  //   console.log(res);
  // }

  // private extreactData(res : Response){
  //   return res.json();
  // }

}
